#include "libs/libs.h"
#include "libs/adc/adc.h"

void ADC_INIT(struct hardware conv) {
	/* Puerto C -------------------------------------------------------------*/
	RCC_AHB1PeriphClockCmd(conv.clock, ENABLE);

	/* PC0 para entrada analogica */
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = conv.pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(conv.port, &GPIO_InitStructure);

	/* Activar ADC1 ----------------------------------------------------------*/
	RCC_APB2PeriphClockCmd(conv.adc.clock, ENABLE);

	/* ADC Common Init -------------------------------------------------------*/
	ADC_CommonStructInit(&ADC_CommonInitStructure);
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div4; // max 36 MHz segun datasheet
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	/* ADC Init ---------------------------------------------------------------*/
	ADC_StructInit(&ADC_InitStructure);
	ADC_InitStructure.ADC_Resolution = conv.adc.resolution;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(conv.adc.nAdc, &ADC_InitStructure);

	/* Establecer la configuración de conversión ------------------------------*/
	ADC_InjectedSequencerLengthConfig(conv.adc.nAdc, 1);
	ADC_SetInjectedOffset(conv.adc.nAdc, conv.adc.injectedChannel, 0);
	ADC_InjectedChannelConfig(conv.adc.nAdc, conv.adc.channel, 1,
	ADC_SampleTime_480Cycles);

	/* Poner en marcha ADC ----------------------------------------------------*/
	ADC_Cmd(conv.adc.nAdc, ENABLE);
}

int ADC_READ(struct hardware conv) {
	ADC_ClearFlag(conv.adc.nAdc, ADC_FLAG_JEOC);
	ADC_SoftwareStartInjectedConv(conv.adc.nAdc);
	while (ADC_GetFlagStatus(conv.adc.nAdc, ADC_FLAG_JEOC) == RESET)
		;
	return ADC_GetInjectedConversionValue(conv.adc.nAdc,
			conv.adc.injectedChannel);
}

float DIG_TO_VOLT(int dig) {
	/* 4095 dig count   --------> 3 V
	 * Actual dig count --------> x V */
	return (float) dig * MAX_VOL_REF / MAX_DIG_REF;
}

float VOLT_TO_TEMP(float volt) {
	/* 0.1 V  --------> -20 deg C
	 * act V  --------> x deg C */
	// Calculate temperature difference per volt
	float tempDiffPerVolt = (float) (MAX_TEMP_REF - MIN_TEMP_REF)
			/ (MAX_TEMP_VOLT - MIN_TEMP_VOLT);

	// Calculate temperature offset
	float tempOffset = (float) MIN_TEMP_REF - (tempDiffPerVolt * MIN_TEMP_VOLT);

	// Convert the voltage to temperature using linear interpolation
	float temperature = (float) (volt * tempDiffPerVolt) + tempOffset;

	return temperature;
}

float SCALE_CURRENT(float volt) {
	/* We de-scale the voltage 5/3 in the circuit *
	 * Here, we scale it back to its original value*/
	return (float) volt * CURRENT_SCALE;
}

float VOLT_TO_CURR(float volt) {
	// Calculate the current difference per volt
	float currDiffPerVolt = (float) (MAX_CURR_REF - MIN_CURR_REF)
			/ (MAX_CURR_VOLT - MIN_CURR_VOLT);

	// Calculate the current offset
	float currOffset = (float) MIN_CURR_REF - (currDiffPerVolt * MIN_CURR_VOLT);

	// Convert the voltage to current using linear interpolation
	float current = (float) (SCALE_CURRENT(volt) * currDiffPerVolt)
			+ currOffset;

	return current;
}
