#ifndef ADC_H
#define ADC_H

#include "libs/hardware/hardware.h"

#define MAX_VOL_REF 3
#define MAX_DIG_REF 4095
#define MAX_TEMP_REF 50
#define MIN_TEMP_REF -20
#define MIN_TEMP_VOLT 0.1
#define MAX_TEMP_VOLT 0.8
#define CURRENT_SCALE 5/3
#define MAX_CURR_REF 5
#define MIN_CURR_REF -5
#define MAX_CURR_VOLT 3.5
#define MIN_CURR_VOLT 1.5


GPIO_InitTypeDef GPIO_InitStructure;
ADC_InitTypeDef ADC_InitStructure;
ADC_CommonInitTypeDef ADC_CommonInitStructure;

void ADC_INIT(struct hardware conv);
int ADC_READ(struct hardware conv);
float DIG_TO_VOLT(int dig);
float VOLT_TO_TEMP(float volt);
float SCALE_CURRENT(float volt);
float VOLT_TO_CURR(float volt);

#endif
