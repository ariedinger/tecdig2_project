#ifndef LIBS_LCD_LCD_H_
#define LIBS_LCD_LCD_H_

#include "libs/libs.h"
#include "libs/hardware/hardware.h"

GPIO_InitTypeDef GPIO_InitStruct;
EXTI_InitTypeDef EXTI_InitStruct;
NVIC_InitTypeDef NVIC_InitStruct;

void EXTI_INIT(struct hardware button);

uint8_t flagEnter, flagDown, flagUp;
int menuOption;

#endif /* LIBS_LCD_LCD_H_ */
