#include "libs/hardware/hardware.h"
#include "libs/leds/leds.h"
#include "libs/lcd/LCD.h"
#include "libs/menu/menu.h"
#include "libs/exti/exti.h"
#include "libs/adc/adc.h"
#include "libs/uart/uart.h"
#include "libs/timers/timers.h"

void INIT_ALL(void) {
	SystemInit();

	/* ~ ~ Red LED ~ ~ */
	ledRed.port = GPIOB;
	ledRed.pin = GPIO_Pin_14;
	ledRed.clock = RCC_AHB1Periph_GPIOB;
	INIT_LEDS(ledRed);

	GPIO_SetBits(ledRed.port, ledRed.pin);

	/* ~ ~ Blue LED ~ ~ */
	ledBlue.port = GPIOB;
	ledBlue.pin = GPIO_Pin_7;
	ledBlue.clock = RCC_AHB1Periph_GPIOB;
	INIT_LEDS(ledBlue);

	GPIO_SetBits(ledBlue.port, ledBlue.pin);

	/* ~ ~ Green LED ~ ~ */
	ledGreen.port = GPIOB;
	ledGreen.pin = GPIO_Pin_0;
	ledGreen.clock = RCC_AHB1Periph_GPIOB;
	INIT_LEDS(ledGreen);

	GPIO_SetBits(ledGreen.port, ledGreen.pin);

	/* Systick config per 1 useg */
	SysTick_Config(SystemCoreClock / 1000);

	/* ~ ~ Timer TIM3 config per 250ms (freq 4Hz) ~ ~ */
	INIT_TIM3(TIM3_FREQ);
	flagTim3 = 0;
	delay = 0;

	/* ~ ~ LCD ~ ~ */
	LCD_init();
	menuOption = 0;
	SCREENA1();

	/* ~ ~ Enter button ~ ~ */
	buttonEnter.port = GPIOD;
	buttonEnter.pin = GPIO_Pin_3;
	buttonEnter.clock = RCC_AHB1Periph_GPIOD;
	buttonEnter.exti.portSource = EXTI_PortSourceGPIOD;
	buttonEnter.exti.pinSource = EXTI_PinSource3;
	buttonEnter.exti.extiLine = EXTI_Line3;
	buttonEnter.exti.extiVector = EXTI3_IRQn;
	EXTI_INIT(buttonEnter);

	flagEnter = 0;

	/* ~ ~ Up button ~ ~ */
	buttonUp.port = GPIOD;
	buttonUp.pin = GPIO_Pin_1;
	buttonUp.clock = RCC_AHB1Periph_GPIOD;
	buttonUp.exti.portSource = EXTI_PortSourceGPIOD;
	buttonUp.exti.pinSource = EXTI_PinSource1;
	buttonUp.exti.extiLine = EXTI_Line1;
	buttonUp.exti.extiVector = EXTI1_IRQn;
	EXTI_INIT(buttonUp);

	flagUp = 0;

	/* ~ ~ Down button ~ ~ */
	buttonDown.port = GPIOD;
	buttonDown.pin = GPIO_Pin_0;
	buttonDown.clock = RCC_AHB1Periph_GPIOD;
	buttonDown.exti.portSource = EXTI_PortSourceGPIOD;
	buttonDown.exti.pinSource = EXTI_PinSource0;
	buttonDown.exti.extiLine = EXTI_Line0;
	buttonDown.exti.extiVector = EXTI0_IRQn;
	EXTI_INIT(buttonDown);

	flagDown = 0;

	/* ~ ~ Temperature sensor 1 ~ ~ */
	tempSen1.port = GPIOC;
	tempSen1.pin = GPIO_Pin_0;
	tempSen1.clock = RCC_AHB1Periph_GPIOC;
	tempSen1.adc.clock = RCC_APB2Periph_ADC1;
	tempSen1.adc.resolution = ADC_Resolution_12b;
	tempSen1.adc.nAdc = ADC1;
	tempSen1.adc.injectedChannel = ADC_InjectedChannel_1;
	tempSen1.adc.channel = ADC_Channel_10;
	ADC_INIT(tempSen1);

	/* ~ ~ Temperature sensor 2 ~ ~ */
	tempSen2.port = GPIOC;
	tempSen2.pin = GPIO_Pin_3;
	tempSen2.clock = RCC_AHB1Periph_GPIOC;
	tempSen2.adc.clock = RCC_APB2Periph_ADC2;
	tempSen2.adc.resolution = ADC_Resolution_12b;
	tempSen2.adc.nAdc = ADC2;
	tempSen2.adc.injectedChannel = ADC_InjectedChannel_1;
	tempSen2.adc.channel = ADC_Channel_13;
	ADC_INIT(tempSen2);

	/* ~ ~ Current sensor ~ ~ */
	currSen.port = GPIOA;
	currSen.pin = GPIO_Pin_3;
	currSen.clock = RCC_AHB1Periph_GPIOA;
	currSen.adc.clock = RCC_APB2Periph_ADC3;
	currSen.adc.resolution = ADC_Resolution_12b;
	currSen.adc.nAdc = ADC3;
	currSen.adc.injectedChannel = ADC_InjectedChannel_1;
	currSen.adc.channel = ADC_Channel_3;
	ADC_INIT(currSen);

	/* ~ ~ RS232 ~ ~ */
	rs232.port = GPIOD;
	rs232.clock = RCC_AHB1Periph_GPIOD;
	rs232.uart.clock = RCC_APB1Periph_USART2;
	rs232.uart.nUart = USART2;
	rs232.uart.nUartGpio = GPIO_AF_USART2;
	rs232.uart.pinTx = GPIO_Pin_5;
	rs232.uart.pinRx = GPIO_Pin_6;
	rs232.uart.pinSourceTx = GPIO_PinSource5;
	rs232.uart.pinSourceRx = GPIO_PinSource6;
	rs232.uart.baudRate = 9600;
	rs232.uart.irq = USART2_IRQn;
	UART_INIT(rs232);

	flagUart = 0;
}
