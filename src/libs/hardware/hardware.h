#ifndef LIBS_HARDWARE_HARDWARE_H_
#define LIBS_HARDWARE_HARDWARE_H_
#include "libs/libs.h"

struct hardware {
    GPIO_TypeDef* port;
    uint16_t pin;
    uint32_t clock;
    struct {
        uint8_t portSource;
        uint8_t pinSource;
        uint32_t extiLine;
        uint8_t extiVector;
    }exti;
    struct {
        uint32_t clock;
        uint32_t resolution;
        ADC_TypeDef* nAdc;
        uint8_t injectedChannel;
        uint8_t channel;
    }adc;
    struct{
    	uint32_t clock;
    	USART_TypeDef* nUart;
    	uint8_t  nUartGpio;
        uint16_t pinTx;
        uint16_t pinRx;
        uint8_t  pinSourceTx;
        uint8_t  pinSourceRx;
        int 	 irq;
        int 	 baudRate;
    }uart;
};

struct hardware ledRed;
struct hardware ledBlue;
struct hardware ledGreen;

struct hardware buttonEnter;
struct hardware buttonUp;
struct hardware buttonDown;

struct hardware tempSen1;
struct hardware tempSen2;
struct hardware currSen;

struct hardware rs232;

void INIT_ALL(void);

#endif /* LIBS_HARDWARE_HARDWARE_H_ */
