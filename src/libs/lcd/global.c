#include "libs/lcd/global.h"

/*
 * Función de delay en microsegundos. Recibe un valor y demora "us" microsegundos en retornar
 * TEMPORIZACION NO TESTEADA
 */
void delay_us(int us){

	us *= 10;
	while(us--);
}


/*
 * Función de delay en milisegundos. Recibe un valor y demora "ms" milisegundos en retornar
 */
void delay_ms(int ms){

	ms += Systick;											// Sumo a ms el tiempo actual de sistick.
	while(ms > Systick);									// Espero que sistick incremente cada 1ms hasta superar a ms.
}

void SysTick_Handler(void) {
	Systick++;
}
