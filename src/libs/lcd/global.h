/*
 * global.h
 *
 *  Created on: Jul 14, 2023
 *      Author: ariedinger
 */

#ifndef LIBS_LCD_GLOBAL_H_
#define LIBS_LCD_GLOBAL_H_

#include "libs/libs.h"
double Systick;

void delay_us(int us);
void delay_ms(int ms);

#endif /* LIBS_LCD_GLOBAL_H_ */
