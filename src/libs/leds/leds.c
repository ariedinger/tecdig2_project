#include "libs/leds/leds.h"

void INIT_LEDS(struct hardware led){
	RCC_AHB1PeriphClockCmd(led.clock, ENABLE);

	GPIO_InitStructure.GPIO_Pin = led.pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	GPIO_Init(led.port, &GPIO_InitStructure);
}
