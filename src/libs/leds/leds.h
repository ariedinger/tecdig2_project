#ifndef LIBS_LEDS_LEDS_H_
#define LIBS_LEDS_LEDS_H_

#include "libs/libs.h"
#include "libs/hardware/hardware.h"

GPIO_InitTypeDef GPIO_InitStructure;

void INIT_LEDS(struct hardware led);

#endif /* LIBS_LEDS_LEDS_H_ */
