/*
 * libs.h
 *
 *  Created on: Jul 9, 2023
 *      Author: ariedinger
 */

#ifndef LIBS_LIBS_H_
#define LIBS_LIBS_H_

#include "stm32f4xx.h"
#include"stm32f4xx_gpio.h"
#include"stm32f4xx_dac.h"
#include"stm32f4xx_rcc.h"
#include"stm32f4xx_tim.h"
#include"stm32f4xx_it.h"
#include"stm32f4xx_dma.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "misc.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#endif /* LIBS_LIBS_H_ */
