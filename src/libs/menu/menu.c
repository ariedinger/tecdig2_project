#include "libs/menu/menu.h"
#include "libs/adc/adc.h"
#include "libs/timers/timers.h"
#include "libs/exti/exti.h"

void MENU_DOWN(int menuOption) {
	switch (menuOption) {
	case SCREEN_A2:
		SCREENA2();
		break;
	case SCREEN_B1:
		SCREENB1();
		break;
	case SCREEN_B2:
		SCREENB2();
		break;
	case SCREEN_C1:
		SCREENC1();
		break;
	case SCREEN_C2:
		SCREENC2();
		break;
	default:
		ERROR_SCREEN();
		break;
	}
}

void MENU_UP(int menuOption) {
	switch (menuOption) {
	case SCREEN_A1:
		SCREENA1();
		break;
	case SCREEN_A2:
		SCREENA2();
		break;
	case SCREEN_B1:
		SCREENB1();
		break;
	case SCREEN_B2:
		SCREENB2();
		break;
	case SCREEN_C1:
		SCREENC1();
		break;
	default:
		ERROR_SCREEN();
		break;
	}
}

void MENU_ENTER(int menuOption) {
	switch (menuOption) {
	case SCREEN_A1:
		FUNC_A1(DONT_SEND_UART);
		break;
	case SCREEN_A2:
		FUNC_A2(DONT_SEND_UART);
		break;
	case SCREEN_B1:
		FUNC_B1(DONT_SEND_UART);
		break;
	case SCREEN_B2:
		FUNC_B2();
		break;
	case SCREEN_C1:
		FUNC_C1();
		break;
	case SCREEN_C2:
		FUNC_C2();
		break;
	default:
		ERROR_SCREEN();
		break;
	}
}

void MENU_UART(void) {
	char rec = USART_ReceiveData(USART2);

	switch (rec) {
	case 'a':
		FUNC_UART();
		break;
	case 'b':
		menuOption = SCREEN_A1;
		FUNC_A1(SEND_UART);
		break;
	case 'c':
		menuOption = SCREEN_A2;
		FUNC_A2(SEND_UART);
		break;
	case 'd':
		menuOption = SCREEN_B1;
		FUNC_B1(SEND_UART);
		break;
	case 'f':
		menuOption = SCREEN_B2;
		FUNC_B2();
		break;
	}
}

void ERROR_SCREEN(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "     ERROR    ");
	LCD_WriteString(0, 1, "ENTRANDA IRRECONOZIDA");
}

void SCREENA1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "-> Upper Temp.");
	LCD_WriteString(0, 1, "   Lower Temp.");
}

void SCREENA2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "   Upper Temp.");
	LCD_WriteString(0, 1, "-> Lower Temp.");
}

void SCREENB1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "-> Print Curr.");
	LCD_WriteString(0, 1, "   Save data SD");
}

void SCREENB2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "   Print Curr.");
	LCD_WriteString(0, 1, "-> Save data SD");
}

void SCREENC1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "-> UART 38400");
	LCD_WriteString(0, 1, "   UART 9600");
}

void SCREENC2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "   UART 38400");
	LCD_WriteString(0, 1, "-> UART 9600");
}

void FUNC_A1(uint8_t sendUart) {
	goAhead = 1;
	LCD_clrscr();
	LCD_WriteString(0, 0, "   Upper Temp.");
	char buffer[15];
	float voltage = DIG_TO_VOLT(ADC_READ(tempSen1));
	sprintf(buffer, "      %d C", (int) VOLT_TO_TEMP(voltage));
	LCD_WriteString(0, 1, buffer);
	if (sendUart) {
		char buf[40];
		sprintf(buf, "Upper Temperature: %d C\n", (int) VOLT_TO_TEMP(voltage));
		UART_SEND_STRING(rs232, buf);
	}
}

void FUNC_A2(uint8_t sendUart) {
	goAhead = 1;
	LCD_clrscr();
	LCD_WriteString(0, 0, "   Lower Temp.");
	char buffer[15];
	float voltage = DIG_TO_VOLT(ADC_READ(tempSen2));
	sprintf(buffer, "      %d C", (int) VOLT_TO_TEMP(voltage));
	LCD_WriteString(0, 1, buffer);
	if (sendUart) {
		char buf[40];
		sprintf(buf, "Lower Temperature: %d C\n", (int) VOLT_TO_TEMP(voltage));
		UART_SEND_STRING(rs232, buf);
	}
}

void FUNC_B1(uint8_t sendUart) {
	goAhead = 1;
	LCD_clrscr();
	LCD_WriteString(0, 0, "  Current Cons.");
	char buffer[15];
	float voltage = DIG_TO_VOLT(ADC_READ(currSen));
	sprintf(buffer, "     %.1f A", VOLT_TO_CURR(voltage));
	LCD_WriteString(0, 1, buffer);
	if (sendUart) {
		char buf[40];
		sprintf(buf, "Current Consumption: %.1f A\n", VOLT_TO_CURR(voltage));
		UART_SEND_STRING(rs232, buf);
	}
}

void FUNC_B2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "Data Saved in SD ");
	LCD_WriteString(0, 1, "  Successfully ");
	char buffer[50];
	float voltageTempSen1 = DIG_TO_VOLT(ADC_READ(tempSen1));
	float voltageTempSen2 = DIG_TO_VOLT(ADC_READ(tempSen2));
	float voltageCurrSen = DIG_TO_VOLT(ADC_READ(currSen));

	sprintf(buffer,
			"Upper Temperature = %d C\n Lower Temperature = %d C\n Current Consuption = %.1f A",
			(int) VOLT_TO_TEMP(voltageTempSen1),
			(int) VOLT_TO_TEMP(voltageTempSen2), VOLT_TO_CURR(voltageCurrSen));

	WRITE_SD(buffer);
}

void FUNC_C1(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, " UART Baud Rate ");
	LCD_WriteString(0, 1, "  Set to 38400 ");
	UART_CHANGE_SPEED(rs232, 38400);
}

void FUNC_C2(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, " UART Baud Rate ");
	LCD_WriteString(0, 1, "  Set to 9600 ");
	UART_CHANGE_SPEED(rs232, 9600);
}

void FUNC_UART(void) {
	LCD_clrscr();
	LCD_WriteString(0, 0, "UART Connection");
	LCD_WriteString(0, 1, " Established");
}

void BUTTON_DOWN(void) {
	flagDown = 0;
	goAhead = 0;
	menuOption++;
	if (menuOption > SCREEN_C2)
		menuOption = SCREEN_C2;
	MENU_DOWN(menuOption);
}

void BUTTON_ENTER(void) {
	flagEnter = 0;
	goAhead = 0;
	MENU_ENTER(menuOption);
}

void BUTTON_UP(void) {
	flagUp = 0;
	goAhead = 0;
	menuOption--;
	if (menuOption < SCREEN_A1)
		menuOption = SCREEN_A1;
	MENU_UP(menuOption);
}

void UART(void) {
	flagUart = 0;
	MENU_UART();
}

void UPDATE_SCREEN(void) {
	delay = 0;
	MENU_ENTER(menuOption);
}
