#ifndef LIBS_MENU_MENU_H_
#define LIBS_MENU_MENU_H_

#include "libs/libs.h"
#include "libs/lcd/LCD.h"
#include "libs/hardware/hardware.h"
#include "libs/uart/uart.h"
#include "libs/sd/sd.h"

#define SCREEN_A1 0
#define SCREEN_A2 1
#define SCREEN_B1 2
#define SCREEN_B2 3
#define SCREEN_C1 4
#define SCREEN_C2 5

#define DONT_SEND_UART 0
#define SEND_UART 1

void MENU_DOWN(int menuOption);
void MENU_UP(int menuOption);
void MENU_ENTER(int menuOption);

void ERROR_SCREEN(void);
void SCREENA1(void);
void SCREENA2(void);
void SCREENB1(void);
void SCREENB2(void);
void SCREENC1(void);
void SCREENC2(void);

void FUNC_A1(uint8_t sendUart);
void FUNC_A2(uint8_t sendUart);
void FUNC_B1(uint8_t sendUart);
void FUNC_B2(void);
void FUNC_C1(void);
void FUNC_C2(void);

void MENU_UART(void);
void FUNC_UART(void);

void BUTTON_DOWN(void);
void BUTTON_UP(void);
void BUTTON_ENTER(void);

void UART(void);

void UPDATE_SCREEN(void);

#endif /* LIBS_MENU_MENU_H_ */
