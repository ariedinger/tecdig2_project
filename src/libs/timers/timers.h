#ifndef LIBS_TIMERS_TIMERS_H_
#define LIBS_TIMERS_TIMERS_H_

#include "libs/libs.h"

#define TIM3_FREQ 4
#define ONE_SEC 4

TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
TIM_OCInitTypeDef TIM_OCInitStructure;
NVIC_InitTypeDef NVIC_InitStructure;

void INIT_TIM3(uint32_t freq);

uint8_t flagTim3;
uint8_t goAhead;
int delay;

#endif /* LIBS_TIMERS_TIMERS_H_ */
