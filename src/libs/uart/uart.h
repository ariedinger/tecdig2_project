#ifndef LIBS_UART_UART_H_
#define LIBS_UART_UART_H_

#include "libs/libs.h"
#include "libs/hardware/hardware.h"

USART_InitTypeDef USART_InitStructure;
GPIO_InitTypeDef GPIO_InitStructure;
NVIC_InitTypeDef NVIC_InitStruct;

void UART_INIT(struct hardware dev);
void UART_SEND_STRING(struct hardware dev, char* message);
void UART_CHANGE_SPEED(struct hardware dev, int speed);

uint8_t flagUart;

#endif /* LIBS_UART_UART_H_ */
