#include "libs/main.h"

int main(void) {

	INIT_ALL();

	while (1) {
		if (flagDown)
			BUTTON_DOWN();
		else if (flagEnter)
			BUTTON_ENTER();
		else if (flagUp)
			BUTTON_UP();
		else if (flagUart)
			UART();
		else if (flagTim3)
			flagTim3 = 0;

		if (delay >= ONE_SEC && goAhead)
			UPDATE_SCREEN();
	}
}
