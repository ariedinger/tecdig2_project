#!/bin/bash

function set_uart_speed() {
    read -p "Enter the new UART speed (9600 or 38400): " speed
    if [ "$speed" == "9600" ] || [ "$speed" == "38400" ]; then
        stty raw -F /dev/ttyUSB0 speed $speed cs8 -cstopb -parenb
        if [ $? -eq 0 ]; then
            echo "UART speed set to $speed."
        else
            echo "Failed to set UART speed."
        fi
    else
        echo "Invalid speed. Please enter either 9600 or 38400."
    fi
}

if [ ! -e "/dev/ttyUSB0" ]; then
    echo "STM32F429ZI is not connected to /dev/ttyUSB0."
    exit 1
else
    echo "STM found on port /dev/ttyUSB0"
fi

echo "We need sudo access to grant write and read permissions to the port."
sudo chmod o+rw /dev/ttyUSB0

if [ $? -ne 0 ]; then
    echo "Failed to grant read and write permissions to /dev/ttyUSB0."
    exit 1
fi

echo "Establishing connection with speed 9600"
stty raw -F /dev/ttyUSB0 speed 9600 cs8 -cstopb -parenb

if [ $? -ne 0 ]; then
    echo "Failed to set UART parameters."
    exit 1
fi

echo -n "a" > /dev/ttyUSB0

if [ $? -ne 0 ]; then
    echo "Failed to send acknowledge symbol."
    exit 1
fi

echo "Handshake completed successfully."

while true; do
    echo "---------------------------------"
    echo "1. Print upper temperature."
    echo "2. Print bottom temperature."
    echo "3. Print current consumption."
    echo "4. Change UART speed."
    echo "5. Save data in SD card."
    echo "6. Exit."

    read -p "Enter your choice (1, 2, 3, 4, 5, or 6): " choice

    case $choice in
        1)
            echo -n "b" > /dev/ttyUSB0
            ;;
        2)
            echo -n "c" > /dev/ttyUSB0
            ;;
        3)
            echo -n "d" > /dev/ttyUSB0
            ;;
        4)
            # Change UART speed
            set_uart_speed
            ;;
        5)
            # Save data in SD card
            echo -n "f" > /dev/ttyUSB0
            ;;
        6)
            # Exit the script
            echo "Exiting."
            exit 0
            ;;
        *)
            echo "Invalid choice. Please enter a valid option (1, 2, 3, 4, 5, or 6)."
            ;;
    esac
done
